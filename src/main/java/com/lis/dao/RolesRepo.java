package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.Roles;

public interface RolesRepo extends JpaRepository<Roles, Long>{

}
