package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.MSG_FROM_ANALYSER;

public interface MSG_FROM_ANALYSER_Repository extends JpaRepository<MSG_FROM_ANALYSER, Long>{

}
