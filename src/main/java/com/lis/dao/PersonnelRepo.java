package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.Personnel;

public interface PersonnelRepo extends JpaRepository<Personnel, Long>{

}
