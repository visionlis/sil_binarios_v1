package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.Automate;

public interface AutomateRepo extends JpaRepository<Automate, Long>{

}
