package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.Resultat;

public interface ResultatRepo extends JpaRepository<Resultat, Long>{

}
