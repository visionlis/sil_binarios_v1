package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.Visite;

public interface VisiteRepo extends JpaRepository<Visite, Long>{

}
