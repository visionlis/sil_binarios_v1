package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.Patient;

public interface PatientRepo extends JpaRepository<Patient, Long>{

}
