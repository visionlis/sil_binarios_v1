package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.Famille;

public interface FamilleRepo extends JpaRepository<Famille, Long>{

}
