package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.Echantillon;

public interface EchantillonRepo extends JpaRepository<Echantillon, Long>{

}
