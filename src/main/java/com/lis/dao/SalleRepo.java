package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.Salle;

public interface SalleRepo extends JpaRepository<Salle, Long>{

}
