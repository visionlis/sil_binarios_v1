package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.Normes;

public interface NormesRepo extends JpaRepository<Normes, Long>{

}
