package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.Trancheage;

public interface TrancheageRepo extends JpaRepository<Trancheage, Long>{

}
