package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.Paramétre;

public interface ParamétreRepo extends JpaRepository<Paramétre, Long>{

}
