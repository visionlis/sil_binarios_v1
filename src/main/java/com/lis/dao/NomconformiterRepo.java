package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.NonConformités;

public interface NomconformiterRepo extends JpaRepository<NonConformités, Long> {

}
