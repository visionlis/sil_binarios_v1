package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.Boxe;

public interface BoxeRepo extends JpaRepository<Boxe, Long>{

}
