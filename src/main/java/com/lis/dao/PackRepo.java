package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.Pack;

public interface PackRepo extends JpaRepository<Pack, Long>{

}
