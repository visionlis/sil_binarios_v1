package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.Resultat_automat;

public interface Resultat_automatRepo extends JpaRepository<Resultat_automat, Long>{

}
