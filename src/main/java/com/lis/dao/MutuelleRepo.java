package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.Mutuelle;

public interface MutuelleRepo extends JpaRepository<Mutuelle, Long>{

}
