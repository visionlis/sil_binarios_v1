package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.Circuit;

public interface CircuitRepo extends JpaRepository<Circuit, Long>{

}
