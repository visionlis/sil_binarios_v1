package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.Valeur;

public interface ValeurRepo extends JpaRepository<Valeur, Long>{

}
