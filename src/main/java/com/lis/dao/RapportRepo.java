package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.Rapport;

public interface RapportRepo extends JpaRepository<Rapport, Long>{

}
