package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.Analyse;

public interface AnalyseRepo extends JpaRepository<Analyse, Long>{

}
