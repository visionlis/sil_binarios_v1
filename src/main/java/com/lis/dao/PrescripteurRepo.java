package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.Prescripteur;

public interface PrescripteurRepo extends JpaRepository<Prescripteur, Long>{

}
