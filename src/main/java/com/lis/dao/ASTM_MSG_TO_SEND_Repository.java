package com.lis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lis.entitie.ASTM_MSG_TO_SEND;

public interface ASTM_MSG_TO_SEND_Repository extends JpaRepository<ASTM_MSG_TO_SEND, Long> {

}
