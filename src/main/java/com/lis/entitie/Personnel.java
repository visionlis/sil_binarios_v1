package com.lis.entitie;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="TYPE_PERSONNE",discriminatorType=DiscriminatorType.STRING,length=50)
public class Personnel implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long ID;
	private String Nom;
	private String prenom;
	private String N_GSM;
	private String Adresse;
	@ManyToMany
	@JoinTable(name="Personne_Groupe")
	private Set<Roles> roles;
	
	public Personnel(String nom, String prenom, String n_GSM, String adresse) {
		super();
		Nom = nom;
		this.prenom = prenom;
		N_GSM = n_GSM;
		Adresse = adresse;
	}
	public Personnel() {
		super();
		
	}
	
	
	public Collection<Roles> getRole() {
		return roles;
	}
	public void setRole(Set<Roles> role) {
		this.roles = role;
	}
	
	public String getNom() {
		return Nom;
	}
	public void setNom(String nom) {
		Nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getN_GSM() {
		return N_GSM;
	}
	public void setN_GSM(String n_GSM) {
		N_GSM = n_GSM;
	}
	public String getAdresse() {
		return Adresse;
	}
	public void setAdresse(String adresse) {
		Adresse = adresse;
	}
	public Long getID() {
		return ID;
	}
	
	
}
