package com.lis.entitie;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Analyse implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id ;
	private String nom_analyse;
	private float prix;
	private String information;
	private String urgent;
	@ManyToOne
	@JoinColumn(name="Id_Famille")
	private Famille famille;
	@ManyToOne
	@JoinColumn(name="Id_Pak_Analysed")
	private Pack pak;
	@OneToOne
	@JoinColumn(name="Id_Resultat")
	private Resultat resultat;
	@ManyToMany(mappedBy="analyse")
	private Set<Visite> visite;
	
	public Analyse() {
		super();
	}
	
	public Analyse(String nom_analyse, float prix, String information, String urgent) {
		super();
		this.nom_analyse = nom_analyse;
		this.prix = prix;
		this.information = information;
		this.urgent = urgent;
	}
	public String getNom_analyse() {
		return nom_analyse;
	}
	public void setNom_analyse(String nom_analyse) {
		this.nom_analyse = nom_analyse;
	}
	public float getPrix() {
		return prix;
	}
	public void setPrix(float prix) {
		this.prix = prix;
	}
	public String getInformation() {
		return information;
	}
	public void setInformation(String information) {
		this.information = information;
	}
	public String getUrgent() {
		return urgent;
	}
	public void setUrgent(String urgent) {
		this.urgent = urgent;
	}
	public Long getId() {
		return id;
	}

	public Famille getFamille() {
		return famille;
	}

	public void setFamille(Famille famille) {
		this.famille = famille;
	}

	public Pack getPak() {
		return pak;
	}

	public void setPak(Pack pak) {
		this.pak = pak;
	}

	public Resultat getResultat() {
		return resultat;
	}

	public void setResultat(Resultat resultat) {
		this.resultat = resultat;
	}

	public Set<Visite> getVisite() {
		return visite;
	}

	public void setVisite(Set<Visite> visite) {
		this.visite = visite;
	}
	
	

}
