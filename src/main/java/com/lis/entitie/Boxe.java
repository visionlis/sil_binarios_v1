package com.lis.entitie;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.ManyToAny;

@Entity
public class Boxe implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private int num_box;
	@ManyToOne
	@JoinColumn(name="Id_salle")
	private Salle salle;
	
	public Salle getSalle() {
		return salle;
	}

	public void setSalle(Salle salle) {
		this.salle = salle;
	}

	public Boxe() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Boxe(int num_box) {
		super();
		this.num_box = num_box;
	}
	public int getNum_box() {
		return num_box;
	}
	public void setNum_box(int num_box) {
		this.num_box = num_box;
	}
	public Long getId() {
		return id;
	}
	

}
