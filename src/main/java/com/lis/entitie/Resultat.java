package com.lis.entitie;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
@Entity
public class Resultat implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String nom_analyse;
	private String resultat_numeric;
	private String resultat_text;
	private String commentaire;
	@OneToOne
	@JoinColumn(name="Id_Valeur")
	private Valeur valeur;
	
	public Resultat() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Resultat(String nom_analyse, String resultat_numeric, String resultat_text, String commentaire) {
		super();
		this.nom_analyse = nom_analyse;
		this.resultat_numeric = resultat_numeric;
		this.resultat_text = resultat_text;
		this.commentaire = commentaire;
	}
	public String getNom_analyse() {
		return nom_analyse;
	}
	public void setNom_analyse(String nom_analyse) {
		this.nom_analyse = nom_analyse;
	}
	public String getResultat_numeric() {
		return resultat_numeric;
	}
	public void setResultat_numeric(String resultat_numeric) {
		this.resultat_numeric = resultat_numeric;
	}
	public String getResultat_text() {
		return resultat_text;
	}
	public void setResultat_text(String resultat_text) {
		this.resultat_text = resultat_text;
	}
	public String getCommentaire() {
		return commentaire;
	}
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
	public Valeur getValeur() {
		return valeur;
	}
	public void setValeur(Valeur valeur) {
		this.valeur = valeur;
	}
	public Long getId() {
		return id;
	}
	
	
	
}
