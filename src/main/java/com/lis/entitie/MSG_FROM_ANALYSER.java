package com.lis.entitie;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class MSG_FROM_ANALYSER implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String nom_analyse;
	private String message_brute;
	private Date recorded_datetime;
	@ManyToOne
	@JoinColumn(name="ID_Resultat")
	private Resultat resultat;
	@ManyToOne
	@JoinColumn(name="ID_Automate")
	private Automate automate;
	public MSG_FROM_ANALYSER() {
		super();
	}
	public MSG_FROM_ANALYSER(String nom_analyse, String message_brute, Date recorded_datetime) {
		super();
		this.nom_analyse = nom_analyse;
		this.message_brute = message_brute;
		this.recorded_datetime = recorded_datetime;
	}
	public String getNom_analyse() {
		return nom_analyse;
	}
	public void setNom_analyse(String nom_analyse) {
		this.nom_analyse = nom_analyse;
	}
	public String getMessage_brute() {
		return message_brute;
	}
	public void setMessage_brute(String message_brute) {
		this.message_brute = message_brute;
	}
	public Date getRecorded_datetime() {
		return recorded_datetime;
	}
	public void setRecorded_datetime(Date recorded_datetime) {
		this.recorded_datetime = recorded_datetime;
	}
	public Resultat getResultat() {
		return resultat;
	}
	public void setResultat(Resultat resultat) {
		this.resultat = resultat;
	}
	public Automate getAutomate() {
		return automate;
	}
	public void setAutomate(Automate automate) {
		this.automate = automate;
	}
	public Long getId() {
		return id;
	}
	

}
