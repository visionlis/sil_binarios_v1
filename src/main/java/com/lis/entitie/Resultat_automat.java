package com.lis.entitie;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
@Entity
public class Resultat_automat implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String nom_analyse;
    private String valeur;
    @ManyToOne
    private Automate automate;
    @ManyToOne
    private Resultat resultat;
	public Resultat_automat() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Resultat_automat(String nom_analyse, String valeur) {
		super();
		this.nom_analyse = nom_analyse;
		this.valeur = valeur;
	}
	public String getNom_analyse() {
		return nom_analyse;
	}
	public void setNom_analyse(String nom_analyse) {
		this.nom_analyse = nom_analyse;
	}
	public String getValeur() {
		return valeur;
	}
	public void setValeur(String valeur) {
		this.valeur = valeur;
	}
	public Automate getAutomate() {
		return automate;
	}
	public void setAutomate(Automate automate) {
		this.automate = automate;
	}
	public Long getId() {
		return id;
	}
	public Resultat getResultat() {
		return resultat;
	}
	public void setResultat(Resultat resultat) {
		this.resultat = resultat;
	}
    

}
