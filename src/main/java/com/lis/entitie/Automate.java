package com.lis.entitie;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
@Entity
public class Automate implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String nom;
	private String type_connection;
	private String sense_connection;
	private String ip;
	private String port;
	private String type_analyse;
	@ManyToMany(mappedBy="automate")
	private Set<Echantillon> echantillon;
	
	public Automate() {
		super();
	}
	public Automate(String nom, String type_connection, String sense_connection, String ip, String port,
			String type_analyse) {
		super();
		this.nom = nom;
		this.type_connection = type_connection;
		this.sense_connection = sense_connection;
		this.ip = ip;
		this.port = port;
		this.type_analyse = type_analyse;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getType_connection() {
		return type_connection;
	}
	public void setType_connection(String type_connection) {
		this.type_connection = type_connection;
	}
	public String getSense_connection() {
		return sense_connection;
	}
	public void setSense_connection(String sense_connection) {
		this.sense_connection = sense_connection;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getType_analyse() {
		return type_analyse;
	}
	public void setType_analyse(String type_analyse) {
		this.type_analyse = type_analyse;
	}
	public Set<Echantillon> getEchantillon() {
		return echantillon;
	}
	public void setEchantillon(Set<Echantillon> echantillon) {
		this.echantillon = echantillon;
	}
	public Long getId() {
		return id;
	}
	
	
	

}
