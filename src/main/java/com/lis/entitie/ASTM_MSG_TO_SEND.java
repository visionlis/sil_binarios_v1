package com.lis.entitie;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class ASTM_MSG_TO_SEND implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private int sent_once;
	private String state;
	private Date recorded_datetime;
	private String header_info;
	private String terminator_info;
	@OneToOne
	@JoinColumn(name="ID_prescripteur")
    private Prescripteur prescripteur;
	@OneToOne
	@JoinColumn(name="ID_visite")
    private Visite visite;
	@OneToOne
	@JoinColumn(name="ID_automate")
    private Automate automate;
	public ASTM_MSG_TO_SEND() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ASTM_MSG_TO_SEND(int sent_once, String state, Date recorded_datetime, String header_info,
			String terminator_info) {
		super();
		this.sent_once = sent_once;
		this.state = state;
		this.recorded_datetime = recorded_datetime;
		this.header_info = header_info;
		this.terminator_info = terminator_info;
	}
	public int getSent_once() {
		return sent_once;
	}
	public void setSent_once(int sent_once) {
		this.sent_once = sent_once;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Date getRecorded_datetime() {
		return recorded_datetime;
	}
	public void setRecorded_datetime(Date recorded_datetime) {
		this.recorded_datetime = recorded_datetime;
	}
	public String getHeader_info() {
		return header_info;
	}
	public void setHeader_info(String header_info) {
		this.header_info = header_info;
	}
	public String getTerminator_info() {
		return terminator_info;
	}
	public void setTerminator_info(String terminator_info) {
		this.terminator_info = terminator_info;
	}
	public Prescripteur getPrescripteur() {
		return prescripteur;
	}
	public void setPrescripteur(Prescripteur prescripteur) {
		this.prescripteur = prescripteur;
	}
	public Visite getVisite() {
		return visite;
	}
	public void setVisite(Visite visite) {
		this.visite = visite;
	}
	public Automate getAutomate() {
		return automate;
	}
	public void setAutomate(Automate automate) {
		this.automate = automate;
	}
	public Long getId() {
		return id;
	}
	
	
    
	

}
