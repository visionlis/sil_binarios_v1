package com.lis.entitie;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;

@Entity
public class NonConformités implements Serializable{
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
private Long ID;
private String nature;
private String cause;
private String notifier_prescripteur;
private String decision;
private String message_prescripteur;
@ManyToOne
@JoinTable(name="Id_Echantillon")
private Echantillon echantillon;

public NonConformités(String nature, String cause, String notifier_prescripteur, String decision,
		String message_prescripteur) {
	super();
	this.nature = nature;
	this.cause = cause;
	this.notifier_prescripteur = notifier_prescripteur;
	this.decision = decision;
	this.message_prescripteur = message_prescripteur;
}
public String getNature() {
	return nature;
}

public void setNature(String nature) {
	this.nature = nature;
}
public String getCause() {
	return cause;
}
public void setCause(String cause) {
	this.cause = cause;
}
public String getNotifier_prescripteur() {
	return notifier_prescripteur;
}
public void setNotifier_prescripteur(String notifier_prescripteur) {
	this.notifier_prescripteur = notifier_prescripteur;
}
public String getDecision() {
	return decision;
}
public void setDecision(String decision) {
	this.decision = decision;
}
public String getMessage_prescripteur() {
	return message_prescripteur;
}
public void setMessage_prescripteur(String message_prescripteur) {
	this.message_prescripteur = message_prescripteur;
}
public Long getID() {
	return ID;
}
public Echantillon getEchantillon() {
	return echantillon;
}
public void setEchantillon(Echantillon echantillon) {
	this.echantillon = echantillon;
}



}

	
	

