package com.lis.entitie;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
@Entity
public class Circuit  implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long ID;
	private String D_H_operation;
	private String opération;
	@OneToOne
	@JoinColumn(name="Id_Visite")
	private Visite visite;
	public Circuit() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Circuit(String d_H_operation, String opération) {
		super();
		D_H_operation = d_H_operation;
		this.opération = opération;
	}
	public String getD_H_operation() {
		return D_H_operation;
	}
	public void setD_H_operation(String d_H_operation) {
		D_H_operation = d_H_operation;
	}
	public String getOpération() {
		return opération;
	}
	public void setOpération(String opération) {
		this.opération = opération;
	}
	public Visite getVisite() {
		return visite;
	}
	public void setVisite(Visite visite) {
		this.visite = visite;
	}
	public Long getID() {
		return ID;
	}
	
	

}
