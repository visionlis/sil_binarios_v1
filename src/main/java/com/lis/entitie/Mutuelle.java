package com.lis.entitie;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Mutuelle {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String nom_organisme;
	private String code_organisme;
	private String num_Adhésion;
	private String formule;
	private Date datefin;
	public Mutuelle() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Mutuelle(String nom_organisme, String code_organisme, String num_Adhésion, String formule, Date datefin) {
		super();
		this.nom_organisme = nom_organisme;
		this.code_organisme = code_organisme;
		this.num_Adhésion = num_Adhésion;
		this.formule = formule;
		this.datefin = datefin;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom_organisme() {
		return nom_organisme;
	}
	public void setNom_organisme(String nom_organisme) {
		this.nom_organisme = nom_organisme;
	}
	public String getNum_Adhésion() {
		return num_Adhésion;
	}
	public void setNum_Adhésion(String num_Adhésion) {
		this.num_Adhésion = num_Adhésion;
	}
	public String getFormule() {
		return formule;
	}
	public void setFormule(String formule) {
		this.formule = formule;
	}
	public Date getDatefin() {
		return datefin;
	}
	public void setDatefin(Date datefin) {
		this.datefin = datefin;
	}
	public String getCode_organisme() {
		return code_organisme;
	}
	public void setCode_organisme(String code_organisme) {
		this.code_organisme = code_organisme;
	}

	

}
