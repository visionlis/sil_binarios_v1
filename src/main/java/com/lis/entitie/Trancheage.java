package com.lis.entitie;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class Trancheage implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String nom_tranche;
	private int val_min;
	private int val_max;
	@ManyToOne
	@JoinColumn(name="Id_Norme")
	private Normes norme;
	public Trancheage() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Trancheage(String nom_tranche, int val_min, int val_max) {
		super();
		this.nom_tranche = nom_tranche;
		this.val_min = val_min;
		this.val_max = val_max;
	}
	public String getNom_tranche() {
		return nom_tranche;
	}
	public void setNom_tranche(String nom_tranche) {
		this.nom_tranche = nom_tranche;
	}
	public int getVal_min() {
		return val_min;
	}
	public void setVal_min(int val_min) {
		this.val_min = val_min;
	}
	public int getVal_max() {
		return val_max;
	}
	public void setVal_max(int val_max) {
		this.val_max = val_max;
	}
	public Normes getNorme() {
		return norme;
	}
	public void setNorme(Normes norme) {
		this.norme = norme;
	}
	public Long getId() {
		return id;
	}
	
	

}
