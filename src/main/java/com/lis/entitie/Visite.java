package com.lis.entitie;

import java.io.Serializable;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


@Entity
public class Visite implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
	
	@ManyToOne
	@JoinColumn(name="Id_Salle")
	private Salle salle;
	
	@OneToOne
	@JoinColumn(name="Id_Rapport")
	private Rapport rapport;
	
	@OneToOne
	@JoinColumn(name="Id_Prescripteur")
	private Prescripteur prescripteur;
	
	@ManyToOne
	@JoinColumn(name="Id_Personnel")
	private Personnel personnel;
	
	@ManyToOne
	@JoinColumn(name="Id_Pateint")
	private Patient patient;	
	
	@ManyToMany
	@JoinTable(name="analyse_visite")
	private Set<Analyse> analyse;
	
	@ManyToOne
	@JoinColumn(name="ID_Mutuelle")
	private Mutuelle mutuelle;
	
    private Date date;
    private int heur;
	private String observation;
    private String type_pation;
    private String ponctualiter;
	private String type_prelevement;
	private String mode_prelevement;
	private String etat_prelevement;
	private Date date_resultat;
	private String mode_Règlement;
	
	public Visite() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Visite(Date date, int heur, String observation, String type_pation,
			String ponctualiter, String type_prelevement, String mode_prelevement, String etat_prelevement,
			Date date_resultat, String mode_Règlement) {
		super();
		this.date = date;
		this.heur = heur;
		this.observation = observation;
		this.type_pation = type_pation;
		this.ponctualiter = ponctualiter;
		this.type_prelevement = type_prelevement;
		this.mode_prelevement = mode_prelevement;
		this.etat_prelevement = etat_prelevement;
		this.date_resultat = date_resultat;
		this.mode_Règlement = mode_Règlement;
	}


	public String getMode_Règlement() {
		return mode_Règlement;
	}

	public void setMode_Règlement(String mode_Règlement) {
		this.mode_Règlement = mode_Règlement;
	}




	public Salle getSalle() {
		return salle;
	}

	public void setSalle(Salle salle) {
		this.salle = salle;
	}

	public Rapport getRapport() {
		return rapport;
	}

	public void setRapport(Rapport rapport) {
		this.rapport = rapport;
	}

	public Prescripteur getPrescripteur() {
		return prescripteur;
	}

	public void setPrescripteur(Prescripteur prescripteur) {
		this.prescripteur = prescripteur;
	}

	public Personnel getPersonnel() {
		return personnel;
	}

	public void setPersonnel(Personnel personnel) {
		this.personnel = personnel;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Set<Analyse> getAnalyse() {
		return analyse;
	}

	public void setAnalyse(Set<Analyse> analyse) {
		this.analyse = analyse;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getHeur() {
		return heur;
	}

	public void setHeur(int heur) {
		this.heur = heur;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public String getType_pation() {
		return type_pation;
	}

	public void setType_pation(String type_pation) {
		this.type_pation = type_pation;
	}

	public String getPonctualiter() {
		return ponctualiter;
	}

	public void setPonctualiter(String ponctualiter) {
		this.ponctualiter = ponctualiter;
	}

	public String getType_prelevement() {
		return type_prelevement;
	}

	public void setType_prelevement(String type_prelevement) {
		this.type_prelevement = type_prelevement;
	}

	public String getMode_prelevement() {
		return mode_prelevement;
	}

	public void setMode_prelevement(String mode_prelevement) {
		this.mode_prelevement = mode_prelevement;
	}

	public String getEtat_prelevement() {
		return etat_prelevement;
	}

	public void setEtat_prelevement(String etat_prelevement) {
		this.etat_prelevement = etat_prelevement;
	}

	public Date getDate_resultat() {
		return date_resultat;
	}

	public void setDate_resultat(Date date_resultat) {
		this.date_resultat = date_resultat;
	}

	public Long getId() {
		return id;
	}

	public Mutuelle getMutuelle() {
		return mutuelle;
	}

	public void setMutuelle(Mutuelle mutuelle) {
		this.mutuelle = mutuelle;
	}


	
}
