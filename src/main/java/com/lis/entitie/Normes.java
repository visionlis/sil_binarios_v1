package com.lis.entitie;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
@Entity
public class Normes implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String unité;
	private int val_NFMN;
	private int val_NFMX;
	private int val_NMMN;
	private int val_NMMX;
	private String enciente;
	@OneToOne
	@JoinColumn(name="Id_Valeur")
	private Valeur valeur;
	public Normes() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Normes(String unité, int val_NFMN, int val_NFMX, int val_NMMN, int val_NMMX, String enciente) {
		super();
		this.unité = unité;
		this.val_NFMN = val_NFMN;
		this.val_NFMX = val_NFMX;
		this.val_NMMN = val_NMMN;
		this.val_NMMX = val_NMMX;
		this.enciente = enciente;
	}
	public String getUnité() {
		return unité;
	}
	public void setUnité(String unité) {
		this.unité = unité;
	}
	public int getVal_NFMN() {
		return val_NFMN;
	}
	public void setVal_NFMN(int val_NFMN) {
		this.val_NFMN = val_NFMN;
	}
	public int getVal_NFMX() {
		return val_NFMX;
	}
	public void setVal_NFMX(int val_NFMX) {
		this.val_NFMX = val_NFMX;
	}
	public int getVal_NMMN() {
		return val_NMMN;
	}
	public void setVal_NMMN(int val_NMMN) {
		this.val_NMMN = val_NMMN;
	}
	public int getVal_NMMX() {
		return val_NMMX;
	}
	public void setVal_NMMX(int val_NMMX) {
		this.val_NMMX = val_NMMX;
	}
	public String getEnciente() {
		return enciente;
	}
	public void setEnciente(String enciente) {
		this.enciente = enciente;
	}
	public Valeur getValeur() {
		return valeur;
	}
	public void setValeur(Valeur valeur) {
		this.valeur = valeur;
	}
	public Long getId() {
		return id;
	}
	
	

}
