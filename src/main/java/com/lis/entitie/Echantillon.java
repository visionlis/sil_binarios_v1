package com.lis.entitie;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Echantillon implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String code_bare;
	private String Commentaire;
	private int répétition;
	private String traité;
	@ManyToOne
	@JoinColumn(name="Id_Analyse")
	private Analyse analyse;
	@ManyToMany
	@JoinTable(name="Echantillon_Automate")
	private Set<Automate> automate;
	
	public Echantillon() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Echantillon(String code_bare, String commentaire, int répétition, String traité) {
		super();
		this.code_bare = code_bare;
		Commentaire = commentaire;
		this.répétition = répétition;
		this.traité = traité;
	}
	public String getCode_bare() {
		return code_bare;
	}
	public void setCode_bare(String code_bare) {
		this.code_bare = code_bare;
	}
	public String getCommentaire() {
		return Commentaire;
	}
	public void setCommentaire(String commentaire) {
		Commentaire = commentaire;
	}
	public int getRépétition() {
		return répétition;
	}
	public void setRépétition(int répétition) {
		this.répétition = répétition;
	}
	public String getTraité() {
		return traité;
	}
	public void setTraité(String traité) {
		this.traité = traité;
	}
	public Analyse getAnalyse() {
		return analyse;
	}
	public void setAnalyse(Analyse analyse) {
		this.analyse = analyse;
	}
	public Long getId() {
		return id;
	}
	public Set<Automate> getAutomate() {
		return automate;
	}
	public void setAutomate(Set<Automate> automate) {
		this.automate = automate;
	}
	

}
