package com.lis.entitie;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Patient implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long iD;
	private String nom;
	private String Prenom;
	private String date_naissance;
	private String sexe;
	private String tél_mobile;
	private String fixe;
	private String adresse;
	private String race;
	private String statu;
	private String civilité;
	public Patient() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Patient(String nOM, String prenom, String date_naissance, String sexe, String tél_mobile, String fixe,
			String adresse, String race, String statu, String civilité) {
		super();
		nom = nOM;
		Prenom = prenom;
		date_naissance = date_naissance;
		sexe = sexe;
		tél_mobile = tél_mobile;
		fixe = fixe;
		adresse = adresse;
		race = race;
		statu = statu;
		civilité = civilité;
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return Prenom;
	}
	public void setPrenom(String prenom) {
		Prenom = prenom;
	}

	public String getFixe() {
		return fixe;
	}
	public void setFixe(String fixe) {
		this.fixe = fixe;
	}

	public String getCivilité() {
		return civilité;
	}
	public void setCivilité(String civilité) {
		this.civilité = civilité;
	}
	public Long getiD() {
		return iD;
	}
	public String getDate_naissance() {
		return date_naissance;
	}
	public void setDate_naissance(String date_naissance) {
		this.date_naissance = date_naissance;
	}
	public String getSexe() {
		return sexe;
	}
	public void setSexe(String sexe) {
		this.sexe = sexe;
	}
	public String getTél_mobile() {
		return tél_mobile;
	}
	public void setTél_mobile(String tél_mobile) {
		this.tél_mobile = tél_mobile;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getRace() {
		return race;
	}
	public void setRace(String race) {
		this.race = race;
	}
	public String getStatu() {
		return statu;
	}
	public void setStatu(String statu) {
		this.statu = statu;
	}
	
		
	
	
	
	
	
}
