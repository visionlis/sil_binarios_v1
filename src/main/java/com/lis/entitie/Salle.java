package com.lis.entitie;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Salle implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id ;
	private int num_salle;
	private int num_étage;
	private String nom_salle;
	
	public Salle() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Salle(int num_salle, int num_étage, String nom_salle) {
		super();
		this.num_salle = num_salle;
		this.num_étage = num_étage;
		this.nom_salle = nom_salle;
	}
	public int getNum_salle() {
		return num_salle;
	}
	public void setNum_salle(int num_salle) {
		this.num_salle = num_salle;
	}
	public int getNum_étage() {
		return num_étage;
	}
	public void setNum_étage(int num_étage) {
		this.num_étage = num_étage;
	}
	public String getNom_salle() {
		return nom_salle;
	}
	public void setNom_salle(String nom_salle) {
		this.nom_salle = nom_salle;
	}
	public Long getId() {
		return id;
	}
	
	

}
