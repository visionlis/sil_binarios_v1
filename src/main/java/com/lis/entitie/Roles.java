package com.lis.entitie;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Roles implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long ID_role;
	private String nom_role;
	@ManyToMany(mappedBy="roles")
	private Set<Personnel> personnel;

	public Roles() {
		super();
	}
	
	public Roles(String nom_role) {
		super();
		this.nom_role = nom_role;
	}
	
	public Collection<Personnel> getPersonnel() {
		return personnel;
	}

	public void setPersonnel(Set<Personnel> personnel) {
		this.personnel = personnel;
	}

	public String getNom_role() {
		return nom_role;
	}
	public void setNom_role(String nom_role) {
		this.nom_role = nom_role;
	}
	public Long getID_role() {
		return ID_role;
	}
	
	

}
