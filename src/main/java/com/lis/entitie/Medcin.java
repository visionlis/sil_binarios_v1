package com.lis.entitie;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Medcin")
public class Medcin extends Personnel{
	
	private String ville;
	private String spécialité;
	private String fix;
	public Medcin() {
		super();
	}

	public Medcin(String nom, String prenom, String n_GSM, String adresse, String ville, String spécialité,
			String fix) {
		super(nom, prenom, n_GSM, adresse);
		this.ville = ville;
		this.spécialité = spécialité;
		this.fix = fix;
	}

	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getSpécialité() {
		return spécialité;
	}
	public void setSpécialité(String spécialité) {
		this.spécialité = spécialité;
	}
	public String getFix() {
		return fix;
	}
	public void setFix(String fix) {
		this.fix = fix;
	}
	
	

}
