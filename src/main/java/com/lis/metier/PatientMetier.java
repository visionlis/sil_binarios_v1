package com.lis.metier;

import java.util.List;

import com.lis.entitie.Patient;

public interface PatientMetier {
	public Patient savePatient(Patient p);
	public List<Patient> listPatient();

}
