package com.lis.metier;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lis.dao.FamilleRepo;
import com.lis.entitie.Famille;

@Service
public class FamilleMetiedrImpl implements FamilleMetier{
	@Autowired
	private FamilleRepo famillerepo;

	@Override
	public Famille saveFamille(Famille a) {
		// TODO Auto-generated method stub
		return famillerepo.save(a);
	}

	@Override
	public List<Famille> listFamille() {
		// TODO Auto-generated method stub
		return famillerepo.findAll();
	}
	

}
