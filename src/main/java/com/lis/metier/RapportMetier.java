package com.lis.metier;

import java.util.List;

import com.lis.entitie.Analyse;
import com.lis.entitie.Rapport;

public interface RapportMetier {
	public Rapport saveRapport(Rapport a);
	public List<Rapport> listRapport();


}
