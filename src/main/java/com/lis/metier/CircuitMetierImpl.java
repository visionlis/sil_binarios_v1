package com.lis.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lis.dao.CircuitRepo;
import com.lis.entitie.Circuit;
@Service
public class CircuitMetierImpl implements CircuitMetier{

	@Autowired
	private CircuitRepo circuitrepo;
	@Override
	public Circuit saveAnalyse(Circuit a) {
		// TODO Auto-generated method stub
		return circuitrepo.save(a);
	}

	@Override
	public List<Circuit> listCircuit() {
		// TODO Auto-generated method stub
		return circuitrepo.findAll();
	}

}
