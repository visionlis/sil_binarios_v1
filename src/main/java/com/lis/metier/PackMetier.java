package com.lis.metier;

import java.util.List;

import com.lis.entitie.Analyse;
import com.lis.entitie.Pack;

public interface PackMetier {
	public Pack savePack(Pack a);
	public List<Pack> listPack();

}
