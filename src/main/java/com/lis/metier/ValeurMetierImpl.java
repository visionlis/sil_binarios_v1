package com.lis.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lis.dao.ValeurRepo;
import com.lis.entitie.Valeur;

@Service
public class ValeurMetierImpl implements ValeurMetier{

	@Autowired
	private ValeurRepo valeurrepo;
	@Override
	public Valeur saveValeur(Valeur a) {
		// TODO Auto-generated method stub
		return valeurrepo.save(a);
	}

	@Override
	public List<Valeur> listValeur() {
		// TODO Auto-generated method stub
		return valeurrepo.findAll();
	}

}
