package com.lis.metier;

import java.util.List;

import com.lis.entitie.Analyse;
import com.lis.entitie.Roles;

public interface RolesMetier {
	public Roles saveRoles(Roles a);
	public List<Roles> listRoles();

}
