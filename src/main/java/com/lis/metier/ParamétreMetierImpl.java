package com.lis.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lis.dao.ParamétreRepo;
import com.lis.entitie.Pack;
import com.lis.entitie.Paramétre;

@Service
public class ParamétreMetierImpl implements ParamétreMetier{
	@Autowired
	private ParamétreRepo parametrerepo;

	@Override
	public Paramétre saveParamétre(Paramétre a) {
		// TODO Auto-generated method stub
		return parametrerepo.save(a);
	}

	@Override
	public List<Paramétre> listParamétre() {
		// TODO Auto-generated method stub
		return parametrerepo.findAll();
	}

}
