package com.lis.metier;

import java.util.List;

import com.lis.entitie.Patient;
import com.lis.entitie.Salle;

public interface SalleMetier {

	public Salle savesalles(Salle s);
	public List<Salle> listSalle();

}
