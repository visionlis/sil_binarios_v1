package com.lis.metier;

import java.util.List;

import com.lis.entitie.Analyse;
import com.lis.entitie.Boxe;

public interface BoxeMetier {
	public Boxe saveBoxe(Boxe b);
	public List<Boxe> listAnalyse();

}
