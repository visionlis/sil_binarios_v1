package com.lis.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lis.dao.RapportRepo;
import com.lis.entitie.Rapport;
@Service
public class RapportMetierImpl implements RapportMetier{
	@Autowired
	private RapportRepo rapportrepo;

	@Override
	public Rapport saveRapport(Rapport a) {
		// TODO Auto-generated method stub
		return rapportrepo.save(a);
	}

	@Override
	public List<Rapport> listRapport() {
		// TODO Auto-generated method stub
		return rapportrepo.findAll();
	}

}
