package com.lis.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lis.dao.PrescripteurRepo;
import com.lis.entitie.Prescripteur;
@Service
public class PrescripteurMetierImpl implements PrescripteurMetier{
	@Autowired
	private PrescripteurRepo prescripteurrepo;

	@Override
	public Prescripteur savePrescripteur(Prescripteur a) {
		// TODO Auto-generated method stub
		return prescripteurrepo.save(a);
	}

	@Override
	public List<Prescripteur> listPrescripteur() {
		// TODO Auto-generated method stub
		return prescripteurrepo.findAll();
	}

}
