package com.lis.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lis.dao.PatientRepo;
import com.lis.entitie.Patient;
@Service
public class PatientMetierImpl implements PatientMetier{
	@Autowired
	private PatientRepo patientrepo;

	@Override
	public Patient savePatient(Patient p) {
		// TODO Auto-generated method stub
		return patientrepo.save(p);
	}

	@Override
	public List<Patient> listPatient() {
		// TODO Auto-generated method stub
		return patientrepo.findAll();
	}

}
