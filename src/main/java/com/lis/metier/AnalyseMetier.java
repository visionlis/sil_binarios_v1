package com.lis.metier;

import java.util.List;

import com.lis.entitie.Analyse;

public interface AnalyseMetier {
	public Analyse saveAnalyse(Analyse a);
	public List<Analyse> listAnalyse();


}
