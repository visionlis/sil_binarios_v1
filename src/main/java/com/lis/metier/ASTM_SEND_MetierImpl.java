package com.lis.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lis.dao.ASTM_MSG_TO_SEND_Repository;
import com.lis.entitie.ASTM_MSG_TO_SEND;

@Service
public class ASTM_SEND_MetierImpl implements ASTM_SEND_Metier{
	@Autowired
	private ASTM_MSG_TO_SEND_Repository astm_send_repo;
	@Override
	public ASTM_MSG_TO_SEND saveASTM_MSG_TO_SEND(ASTM_MSG_TO_SEND a) {
		// TODO Auto-generated method stub
		return astm_send_repo.save(a);
	}

	@Override
	public List<ASTM_MSG_TO_SEND> listASTM_MSG_TO_SEND() {
		// TODO Auto-generated method stub
		return astm_send_repo.findAll();
	}

}
