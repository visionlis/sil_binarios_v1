package com.lis.metier;

import java.util.List;

import org.springframework.stereotype.Service;

import com.lis.dao.Resultat_automatRepo;
import com.lis.entitie.Resultat_automat;

@Service
public class Resultat_automatMetierImpl implements Resultat_automatMetier{
	
	private Resultat_automatRepo resultat_automatrepot;

	@Override
	public Resultat_automat saveResultat_automat(Resultat_automat a) {
		// TODO Auto-generated method stub
		return resultat_automatrepot.save(a);
	}

	@Override
	public List<Resultat_automat> listResultat_automat() {
		// TODO Auto-generated method stub
		return resultat_automatrepot.findAll();
	}

}
