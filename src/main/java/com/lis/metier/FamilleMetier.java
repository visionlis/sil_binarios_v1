package com.lis.metier;

import java.util.List;

import com.lis.entitie.Analyse;
import com.lis.entitie.Famille;

public interface FamilleMetier {

	public Famille saveFamille(Famille a);
	public List<Famille> listFamille();
}
