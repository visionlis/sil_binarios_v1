package com.lis.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lis.dao.RolesRepo;
import com.lis.entitie.Roles;

@Service
public class RolesMetierImpl implements RolesMetier{

	@Autowired
	private RolesRepo rolesrepo;
	@Override
	public Roles saveRoles(Roles a) {
		// TODO Auto-generated method stub
		return rolesrepo.save(a);
	}

	@Override
	public List<Roles> listRoles() {
		// TODO Auto-generated method stub
		return rolesrepo.findAll();
	}

}
