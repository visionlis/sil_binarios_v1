package com.lis.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lis.dao.VisiteRepo;
import com.lis.entitie.Patient;
import com.lis.entitie.Visite;

@Service
public class VisiteMetierImpl implements VisiteMetier{
	@Autowired
	private VisiteRepo visiterepo;

	@Override
	public Visite saveVisite(Visite v) {
		// TODO Auto-generated method stub
		return visiterepo.save(v);
	}
	
	@Override
	public List<Visite> listVisite() {
		// TODO Auto-generated method stub
		return visiterepo.findAll();
	}
	
	

}
