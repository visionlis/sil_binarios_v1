package com.lis.metier;
import java.util.List;

import com.lis.entitie.Analyse;
import com.lis.entitie.Echantillon;

public interface EchantillonMetier {

	public Echantillon saveEchantillon(Echantillon a);
	public List<Echantillon> listEchantillon();
}
