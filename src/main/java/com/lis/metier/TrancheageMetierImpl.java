package com.lis.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lis.dao.TrancheageRepo;
import com.lis.entitie.Trancheage;
@Service
public class TrancheageMetierImpl implements TrancheageMetier{
	@Autowired
	private TrancheageRepo trancheagerepo;

	@Override
	public Trancheage saveTrancheage(Trancheage a) {
		// TODO Auto-generated method stub
		return trancheagerepo.save(a);
	}

	@Override
	public List<Trancheage> listTrancheage() {
		// TODO Auto-generated method stub
		return trancheagerepo.findAll();
	}

}
