package com.lis.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lis.dao.BoxeRepo;
import com.lis.entitie.Boxe;
@Service
public class BoxeMetierImlpl implements BoxeMetier{

	@Autowired
	private BoxeRepo boxerepo;
	
	@Override
	public Boxe saveBoxe(Boxe b) {
		// TODO Auto-generated method stub
		return boxerepo.save(b);
	}

	@Override
	public List<Boxe> listAnalyse() {
		// TODO Auto-generated method stub
		return boxerepo.findAll();
	}

}
