package com.lis.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lis.dao.MutuelleRepo;
import com.lis.entitie.Analyse;
import com.lis.entitie.Mutuelle;

@Service
public class MutuelleMetierImpl implements MutuelleMetier{

	@Autowired
	private MutuelleRepo mutuellerepo;
	@Override
	public Mutuelle saveMutuelle(Mutuelle a) {
		// TODO Auto-generated method stub
		return mutuellerepo.save(a);
	}

	@Override
	public List<Mutuelle> listMutuelle() {
		// TODO Auto-generated method stub
		return mutuellerepo.findAll();
	}

}
