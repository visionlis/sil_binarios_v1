package com.lis.metier;

import java.util.List;

import com.lis.entitie.Analyse;
import com.lis.entitie.Automate;

public interface AutomateMetier {
	public Automate saveAutomate(Automate a);
	public List<Automate> listAutomate();

}
