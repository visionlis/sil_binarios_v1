package com.lis.metier;

import java.util.List;

import com.lis.entitie.Analyse;
import com.lis.entitie.Trancheage;

public interface TrancheageMetier {
	public Trancheage saveTrancheage(Trancheage a);
	public List<Trancheage> listTrancheage();

}
