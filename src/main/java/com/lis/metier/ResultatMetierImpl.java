package com.lis.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lis.dao.ResultatRepo;
import com.lis.entitie.Resultat;
@Service
public class ResultatMetierImpl implements ResultatMetier{
	
	@Autowired
	private ResultatRepo resultatrepo;
	@Override
	public Resultat saveResultat(Resultat a) {
		// TODO Auto-generated method stub
		return resultatrepo.save(a);
	}

	@Override
	public List<Resultat> listResultat() {
		// TODO Auto-generated method stub
		return resultatrepo.findAll();
	}

}
