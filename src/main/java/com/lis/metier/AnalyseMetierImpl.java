package com.lis.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lis.dao.AnalyseRepo;
import com.lis.entitie.Analyse;

@Service
public class AnalyseMetierImpl implements AnalyseMetier{
	
	@Autowired
	private AnalyseRepo analyserepo;
	@Override
	public Analyse saveAnalyse(Analyse a) {
		// TODO Auto-generated method stub
		return analyserepo.save(a);
	}

	@Override
	public List<Analyse> listAnalyse() {
		// TODO Auto-generated method stub
		return analyserepo.findAll();
	}
	

}
