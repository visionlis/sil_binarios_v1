package com.lis.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lis.dao.PackRepo;
import com.lis.entitie.Pack;

@Service
public class PackMetierImpl implements PackMetier{
	@Autowired
	private PackRepo packrepo;

	@Override
	public Pack savePack(Pack a) {
		// TODO Auto-generated method stub
		return packrepo.save(a);
	}

	@Override
	public List<Pack> listPack() {
		// TODO Auto-generated method stub
		return packrepo.findAll();
	}
	

}
