package com.lis.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lis.dao.AutomateRepo;
import com.lis.entitie.Automate;

@Service
public class AutomateMetireIpml implements AutomateMetier{

	@Autowired
	private AutomateRepo automaterespo;
	@Override
	public Automate saveAutomate(Automate a) {
		// TODO Auto-generated method stub
		return automaterespo.save(a);
	}

	@Override
	public List<Automate> listAutomate() {
		// TODO Auto-generated method stub
		return automaterespo.findAll();
	}

}
