package com.lis.metier;

import java.util.List;

import com.lis.entitie.Analyse;
import com.lis.entitie.Personnel;

public interface PersonnelMetier {
	public Personnel savePersonnel(Personnel a);
	public List<Personnel> listPersonnel();

}
