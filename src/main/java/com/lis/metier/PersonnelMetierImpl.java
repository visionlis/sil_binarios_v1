package com.lis.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lis.dao.PersonnelRepo;
import com.lis.entitie.Personnel;

@Service
public class PersonnelMetierImpl implements PersonnelMetier{
	@Autowired
	private PersonnelRepo personnelrepop;

	@Override
	public Personnel savePersonnel(Personnel a) {
		// TODO Auto-generated method stub
		return personnelrepop.save(a);
	}

	@Override
	public List<Personnel> listPersonnel() {
		// TODO Auto-generated method stub
		return personnelrepop.findAll();
	}

}
