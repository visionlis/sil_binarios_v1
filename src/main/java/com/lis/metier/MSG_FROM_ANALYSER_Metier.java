package com.lis.metier;

import java.util.List;

import com.lis.entitie.Analyse;
import com.lis.entitie.MSG_FROM_ANALYSER;

public interface MSG_FROM_ANALYSER_Metier {
	public MSG_FROM_ANALYSER saveMSG_FROM_ANALYSER(MSG_FROM_ANALYSER a);
	public List<MSG_FROM_ANALYSER> listMSG_FROM_ANALYSER();

}
