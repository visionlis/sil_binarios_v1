package com.lis.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lis.dao.SalleRepo;
import com.lis.entitie.Salle;

@Service
public class SalleMetierImpl implements SalleMetier{
	@Autowired
	private SalleRepo sallerepo;

	@Override
	public Salle savesalles(Salle s) {
		// TODO Auto-generated method stub
		return sallerepo.save(s);
	}

	@Override
	public List<Salle> listSalle() {
		// TODO Auto-generated method stub
		return sallerepo.findAll();
	}


}
