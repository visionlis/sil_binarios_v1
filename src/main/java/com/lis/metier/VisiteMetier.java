package com.lis.metier;

import java.util.List;

import com.lis.entitie.Patient;
import com.lis.entitie.Visite;

public interface VisiteMetier {
	public Visite saveVisite(Visite v);
	public List<Visite> listVisite();

}
