package com.lis.metier;

import java.util.List;

import com.lis.entitie.Analyse;
import com.lis.entitie.Mutuelle;

public interface MutuelleMetier {
	public Mutuelle saveMutuelle(Mutuelle a);
	public List<Mutuelle> listMutuelle();

}
