package com.lis.metier;

import java.util.List;

import com.lis.entitie.Analyse;
import com.lis.entitie.Normes;

public interface NormesMetier {
	public Normes saveNormes(Normes a);
	public List<Normes> listNormes();

}
