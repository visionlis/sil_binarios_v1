package com.lis.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.lis.dao.EchantillonRepo;
import com.lis.entitie.Echantillon;

@Service
public class EchantillonMetierImpl implements EchantillonMetier{
	@Autowired
	private EchantillonRepo echantillonrespo;


	@Override
	public Echantillon saveEchantillon(Echantillon a) {
		// TODO Auto-generated method stub
		return echantillonrespo.save(a);
	}

	@Override
	public List<Echantillon> listEchantillon() {
		// TODO Auto-generated method stub
		return echantillonrespo.findAll();
	}

}
