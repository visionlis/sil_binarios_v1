package com.lis.metier;

import java.util.List;

import com.lis.entitie.Analyse;
import com.lis.entitie.Prescripteur;

public interface PrescripteurMetier {
	public Prescripteur savePrescripteur(Prescripteur a);
	public List<Prescripteur> listPrescripteur();

}
