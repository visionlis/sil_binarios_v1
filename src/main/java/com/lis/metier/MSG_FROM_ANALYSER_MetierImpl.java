package com.lis.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lis.dao.MSG_FROM_ANALYSER_Repository;
import com.lis.entitie.MSG_FROM_ANALYSER;
@Service
public class MSG_FROM_ANALYSER_MetierImpl implements MSG_FROM_ANALYSER_Metier{
	@Autowired
	private MSG_FROM_ANALYSER_Repository msg_from_analyser;

	@Override
	public MSG_FROM_ANALYSER saveMSG_FROM_ANALYSER(MSG_FROM_ANALYSER a) {
		// TODO Auto-generated method stub
		return msg_from_analyser.save(a);
	}

	@Override
	public List<MSG_FROM_ANALYSER> listMSG_FROM_ANALYSER() {
		// TODO Auto-generated method stub
		return msg_from_analyser.findAll();
	}

}
