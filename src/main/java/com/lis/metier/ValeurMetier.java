package com.lis.metier;

import java.util.List;

import com.lis.entitie.Analyse;
import com.lis.entitie.Valeur;

public interface ValeurMetier {
	public Valeur saveValeur(Valeur a);
	public List<Valeur> listValeur();

}
