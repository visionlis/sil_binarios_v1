package com.lis.metier;

import java.util.List;

import com.lis.entitie.Analyse;
import com.lis.entitie.NonConformités;

public interface NonConformiterMetier {
	public NonConformités saveAnalyse(NonConformités n);
	public List<NonConformités> listNonConformités();
}
