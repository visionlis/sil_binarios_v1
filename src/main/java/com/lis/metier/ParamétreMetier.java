package com.lis.metier;

import java.util.List;

import com.lis.entitie.Analyse;
import com.lis.entitie.Paramétre;

public interface ParamétreMetier {
	public Paramétre saveParamétre(Paramétre a);
	public List<Paramétre> listParamétre();
}
