package com.lis.metier;

import java.util.List;

import com.lis.entitie.ASTM_MSG_TO_SEND;
import com.lis.entitie.Analyse;

public interface ASTM_SEND_Metier {
	public ASTM_MSG_TO_SEND saveASTM_MSG_TO_SEND(ASTM_MSG_TO_SEND a);
	public List<ASTM_MSG_TO_SEND> listASTM_MSG_TO_SEND();

}
