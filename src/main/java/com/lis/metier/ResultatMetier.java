package com.lis.metier;

import java.util.List;

import com.lis.entitie.Analyse;
import com.lis.entitie.Resultat;

public interface ResultatMetier {
	public  Resultat saveResultat( Resultat a);
	public List<Resultat> listResultat();

}
