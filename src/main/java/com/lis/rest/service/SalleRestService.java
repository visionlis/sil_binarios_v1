package com.lis.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lis.entitie.Salle;
import com.lis.metier.SalleMetier;

@RestController
public class SalleRestService {
	
	@Autowired
	private SalleMetier sallemetier;

	@RequestMapping(value="/salles",method=RequestMethod.POST)
	public Salle savesalles(@RequestBody Salle s) {
		return sallemetier.savesalles(s);
	}
	@RequestMapping(value="/salles",method=RequestMethod.GET)
	public List<Salle> listSalle() {
		return sallemetier.listSalle();
	}

	
}
