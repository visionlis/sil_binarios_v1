package com.lis.rest.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.lis.entitie.Patient;
import com.lis.metier.PatientMetier;

@RestController
public class PatientRestService{

	@Autowired
	private PatientMetier patientmetier;

	@RequestMapping(value="/patients",method=RequestMethod.POST)
	public Patient savePatient(@RequestBody Patient p) {
		return patientmetier.savePatient(p);
	}
	@RequestMapping(value="/patients",method=RequestMethod.GET)
	public List<Patient> listPatient() {
		return patientmetier.listPatient();
	}

}
