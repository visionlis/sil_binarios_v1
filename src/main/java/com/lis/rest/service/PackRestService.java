package com.lis.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lis.entitie.Pack;
import com.lis.metier.PackMetier;
@RestController
public class PackRestService {
	@Autowired
	private PackMetier packmetier;

	@RequestMapping(value="packs",method=RequestMethod.POST)
	public Pack savePack(@RequestBody Pack a) {
		return packmetier.savePack(a);
	}

	@RequestMapping(value="packs",method=RequestMethod.GET)
	public List<Pack> listPack() {
		return packmetier.listPack();
	}

}
