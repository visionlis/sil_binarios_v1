package com.lis.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lis.entitie.Paramétre;
import com.lis.metier.ParamétreMetier;
@RestController
public class ParamétreRestService {
	@Autowired
	private ParamétreMetier paramétremetier;

	@RequestMapping(value="/paramétres",method=RequestMethod.POST)
	public Paramétre saveParamétre(@RequestBody Paramétre a) {
		return paramétremetier.saveParamétre(a);
	}

	@RequestMapping(value="/paramétres",method=RequestMethod.GET)
	public List<Paramétre> listParamétre() {
		return paramétremetier.listParamétre();
	}

}
