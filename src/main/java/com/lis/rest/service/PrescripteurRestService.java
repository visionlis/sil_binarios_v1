package com.lis.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lis.entitie.Prescripteur;
import com.lis.metier.PrescripteurMetier;

@RestController
public class PrescripteurRestService {
	@Autowired
	private PrescripteurMetier prescripteurmitier;

	@RequestMapping(value="/prescripteurs",method=RequestMethod.POST)
	public Prescripteur savePrescripteur(Prescripteur a) {
		return prescripteurmitier.savePrescripteur(a);
	}

	@RequestMapping(value="/prescripteurs",method=RequestMethod.GET)
	public List<Prescripteur> listPrescripteur() {
		return prescripteurmitier.listPrescripteur();
	}

}
