package com.lis.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lis.entitie.Famille;
import com.lis.metier.FamilleMetier;

@RestController
public class FamilleRestService {
	@Autowired
	private FamilleMetier famillemetier;

	@RequestMapping(value="/familles",method=RequestMethod.POST)
	public Famille saveFamille(Famille a) {
		return famillemetier.saveFamille(a);
	}

	@RequestMapping(value="/familles",method=RequestMethod.GET)
	public List<Famille> listFamille() {
		return famillemetier.listFamille();
	}
	

}
