package com.lis.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lis.entitie.Echantillon;
import com.lis.metier.EchantillonMetier;
@RestController
public class EchantillonRestService {
	@Autowired
	private EchantillonMetier echantillonmetier;

	@RequestMapping(value="/echantillons",method=RequestMethod.POST)
	public Echantillon saveEchantillon(@RequestBody Echantillon a) {
		return echantillonmetier.saveEchantillon(a);
	}

	@RequestMapping(value="/echantillons",method=RequestMethod.GET)
	public List<Echantillon> listEchantillon() {
		return echantillonmetier.listEchantillon();
	}

}
