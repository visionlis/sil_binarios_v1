package com.lis.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lis.entitie.Roles;
import com.lis.metier.RolesMetier;
@RestController
public class RolesRestService {
	@Autowired
	private RolesMetier rolemetier;

	@RequestMapping(value="/roles",method=RequestMethod.POST)
	public Roles saveRoles(@RequestBody Roles a) {
		return rolemetier.saveRoles(a);
	}

	@RequestMapping(value="/roles",method=RequestMethod.GET)
	public List<Roles> listRoles() {
		return rolemetier.listRoles();
	}

}
