package com.lis.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lis.entitie.Valeur;
import com.lis.metier.ValeurMetier;

@RestController
public class ValeurRestService {
	@Autowired
	private ValeurMetier valeurmetier;

	@RequestMapping(value="/valeurs",method=RequestMethod.POST)
	public Valeur saveValeur(@RequestBody Valeur a) {
		return valeurmetier.saveValeur(a);
	}

	@RequestMapping(value="/valeur",method=RequestMethod.GET)
	public List<Valeur> listValeur() {
		return valeurmetier.listValeur();
	}
	
	

}
