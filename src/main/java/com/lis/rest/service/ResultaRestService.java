package com.lis.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lis.entitie.Resultat;
import com.lis.metier.ResultatMetier;
@RestController
public class ResultaRestService {
	@Autowired
	private ResultatMetier resultatmetier;

	@RequestMapping(value="/resultats",method=RequestMethod.POST)
	public Resultat saveResultat(Resultat a) {
		return resultatmetier.saveResultat(a);
	}

	@RequestMapping(value="/resultats",method=RequestMethod.GET)
	public List<Resultat> listResultat() {
		return resultatmetier.listResultat();
	}

}
