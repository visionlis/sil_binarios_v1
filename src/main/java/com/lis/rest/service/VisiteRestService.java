package com.lis.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lis.entitie.Visite;
import com.lis.metier.VisiteMetier;

@RestController
public class VisiteRestService {
	@Autowired
	private VisiteMetier visitemetier;

	@RequestMapping(value="/visites",method=RequestMethod.POST)
	public Visite saveVisite(@RequestBody Visite v) {
		return visitemetier.saveVisite(v);
	}

	@RequestMapping(value="/visites",method=RequestMethod.GET)
	public List<Visite> listVisite() {
		return visitemetier.listVisite();
	}

}
