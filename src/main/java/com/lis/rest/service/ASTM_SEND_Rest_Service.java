package com.lis.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lis.entitie.ASTM_MSG_TO_SEND;
import com.lis.metier.ASTM_SEND_Metier;

@RestController
public class ASTM_SEND_Rest_Service {
	@Autowired
	private ASTM_SEND_Metier astm_send_metier;

	@RequestMapping(value="/astmsends",method=RequestMethod.POST)
	public ASTM_MSG_TO_SEND saveASTM_MSG_TO_SEND(@RequestBody ASTM_MSG_TO_SEND a) {
		return astm_send_metier.saveASTM_MSG_TO_SEND(a);
	}

	@RequestMapping(value="/astmsends",method=RequestMethod.GET)
	public List<ASTM_MSG_TO_SEND> listASTM_MSG_TO_SEND() {
		return astm_send_metier.listASTM_MSG_TO_SEND();
	}

}
