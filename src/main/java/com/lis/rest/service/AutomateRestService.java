package com.lis.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lis.entitie.Automate;
import com.lis.metier.AutomateMetier;
@RestController
public class AutomateRestService {
	@Autowired
	private AutomateMetier automatemetier;

	@RequestMapping(value="/automates",method=RequestMethod.POST)
	public Automate saveAutomate(@RequestBody Automate a) {
		return automatemetier.saveAutomate(a);
	}

	@RequestMapping(value="/automates",method=RequestMethod.GET)
	public List<Automate> listAutomate() {
		return automatemetier.listAutomate();
	}
	

}
