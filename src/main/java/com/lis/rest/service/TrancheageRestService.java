package com.lis.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lis.entitie.Trancheage;
import com.lis.metier.TrancheageMetier;
@RestController
public class TrancheageRestService {
	@Autowired
	private TrancheageMetier trancheage;

	@RequestMapping(value="/trancheages",method=RequestMethod.POST)
	public Trancheage saveTrancheage(@RequestBody Trancheage a) {
		return trancheage.saveTrancheage(a);
	}

	@RequestMapping(value="/trancheages",method=RequestMethod.GET)
	public List<Trancheage> listTrancheage() {
		return trancheage.listTrancheage();
	}

}
