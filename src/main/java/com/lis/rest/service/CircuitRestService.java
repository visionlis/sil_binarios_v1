package com.lis.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lis.entitie.Circuit;
import com.lis.metier.CircuitMetier;
@RestController
public class CircuitRestService {
	@Autowired
	private CircuitMetier circuitmetier;

	@RequestMapping(value="/circuits",method=RequestMethod.POST)
	public Circuit saveAnalyse(@RequestBody Circuit a) {
		return circuitmetier.saveAnalyse(a);
	}

	@RequestMapping(value="/circuits",method=RequestMethod.GET)
	public List<Circuit> listCircuit() {
		return circuitmetier.listCircuit();
	}
}
