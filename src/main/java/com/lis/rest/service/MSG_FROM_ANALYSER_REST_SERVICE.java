package com.lis.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lis.entitie.MSG_FROM_ANALYSER;
import com.lis.metier.MSG_FROM_ANALYSER_Metier;
@RestController
public class MSG_FROM_ANALYSER_REST_SERVICE {
	@Autowired
	private MSG_FROM_ANALYSER_Metier msg_from_analyser;

	@RequestMapping(value="/msganalysers",method=RequestMethod.POST)
	public MSG_FROM_ANALYSER saveMSG_FROM_ANALYSER(MSG_FROM_ANALYSER a) {
		return msg_from_analyser.saveMSG_FROM_ANALYSER(a);
	}

	@RequestMapping(value="/msganalysers",method=RequestMethod.GET)
	public List<MSG_FROM_ANALYSER> listMSG_FROM_ANALYSER() {
		return msg_from_analyser.listMSG_FROM_ANALYSER();
	}
	

}
