package com.lis.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lis.entitie.Resultat_automat;
import com.lis.metier.Resultat_automatMetier;

@RestController
public class Resultat_automatRestService {
	@Autowired
	private Resultat_automatMetier resultat_automat;

	@RequestMapping(value="/Resultatsautomats",method=RequestMethod.POST)
	public Resultat_automat saveResultat_automat(@RequestBody Resultat_automat a) {
		return resultat_automat.saveResultat_automat(a);
	}

	@RequestMapping(value="/Resultatsautomats",method=RequestMethod.GET)
	public List<Resultat_automat> listResultat_automat() {
		return resultat_automat.listResultat_automat();
	}

}
