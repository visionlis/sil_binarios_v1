package com.lis.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lis.entitie.Mutuelle;
import com.lis.metier.MutuelleMetier;
@RestController
public class MutuelleRestService {
	@Autowired
	private MutuelleMetier mutuellemetier;

	@RequestMapping(value="/mutuelles",method=RequestMethod.POST)
	public Mutuelle saveMutuelle(@RequestBody Mutuelle a) {
		return mutuellemetier.saveMutuelle(a);
	}

	@RequestMapping(value="/mutuelles",method=RequestMethod.GET)
	public List<Mutuelle> listMutuelle() {
		return mutuellemetier.listMutuelle();
	}
	

}
