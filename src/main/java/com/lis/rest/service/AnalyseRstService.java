package com.lis.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lis.entitie.Analyse;
import com.lis.metier.AnalyseMetier;

@RestController
public class AnalyseRstService {
	@Autowired
	private AnalyseMetier analysemetier;

	@RequestMapping(value="/analyses",method=RequestMethod.POST)
	public Analyse saveAnalyse(@RequestBody Analyse a) {
		return analysemetier.saveAnalyse(a);
	}

	@RequestMapping(value="/analyses",method=RequestMethod.GET)
	public List<Analyse> listAnalyse() {
		return analysemetier.listAnalyse();
	}
	

}
