package com.lis.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lis.entitie.Personnel;
import com.lis.metier.PersonnelMetier;

@RestController
public class PersonnelRestService {
	@Autowired
	private PersonnelMetier pesonnelmetier;

	@RequestMapping(value="/pesonnels",method=RequestMethod.POST)
	public Personnel savePersonnel(@RequestBody Personnel a) {
		return pesonnelmetier.savePersonnel(a);
	}

	@RequestMapping(value="/pesonnels",method=RequestMethod.GET)
	public List<Personnel> listPersonnel() {
		return pesonnelmetier.listPersonnel();
	}
	

}
