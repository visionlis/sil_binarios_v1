package com.lis.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lis.entitie.Boxe;
import com.lis.metier.BoxeMetier;

@RestController
public class BoxeRestService {
	@Autowired
	private BoxeMetier boxemetier;

	@RequestMapping(value="/boxes",method=RequestMethod.POST)
	public Boxe saveBoxe(@RequestBody Boxe b) {
		return boxemetier.saveBoxe(b);
	}

	@RequestMapping(value="/boxes",method=RequestMethod.GET)
	public List<Boxe> listAnalyse() {
		return boxemetier.listAnalyse();
	}

}
