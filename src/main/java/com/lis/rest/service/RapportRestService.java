package com.lis.rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lis.entitie.Rapport;
import com.lis.metier.RapportMetier;
@RestController
public class RapportRestService {
	@Autowired
	private RapportMetier rapportmeetier;

	@RequestMapping(value="/rapports",method=RequestMethod.POST)
	public Rapport saveRapport(@RequestBody Rapport a) {
		return rapportmeetier.saveRapport(a);
	}

	@RequestMapping(value="/rapports",method=RequestMethod.GET)
	public List<Rapport> listRapport() {
		return rapportmeetier.listRapport();
	}
	
	

}
