package com.lis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LisBinarios2Application {

	public static void main(String[] args) {
		SpringApplication.run(LisBinarios2Application.class, args);
	}
}
